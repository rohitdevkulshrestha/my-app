#! /bin/sh
echo "Running cleanup"
rm -r -f ./coverage ./node_modules .git/hooks ./.husky ./build/
echo "Installing node modules"
npm install
#npx install-peerdeps --dev eslint-config-airbnb
npm install eslint-config-airbnb@latest eslint@latest eslint-plugin-import@latest eslint-plugin-jsx-a11y@latest eslint-plugin-react@latest eslint-plugin-react-hooks@latest --save-devclear
#npx husky add ".husky/pre-commit" \"npx prettier --write .\"
#npx husky add ".husky/pre-commit" \"npm run lint -- --fix\"
#npx husky add ".husky/pre-commit" \"npm test\"
echo "Running code prettier"
npx prettier --write .
echo "Running eslinter"
npm run lint -- --fix
echo "Run Tests"
npm test
